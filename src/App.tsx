import React, { useEffect, useState } from 'react';
import './App.css';
import { Map, TileLayer, Marker, Circle } from 'react-leaflet'
import { getEarthquakes, convertLatLngToDegrees, handleChange, handleMapClick, showSlideValue } from './service/api'
import Earthquake from './Earthquake'
import { LeafletMouseEvent } from 'leaflet'

import Icon from './assets/Icon'
import { Slider, Typography } from '@material-ui/core'
import EarthquakeItemList from './EarthquakeItemList';

function App() {

  // armazena os valores de max e min de magnitude para mostrar no slider
  const [magnitudes, setMagnitudes] = useState<number[]>([7, 10])
  // armazena os valores de max e min de magnitude para realizar a busca
  const [magnitudesSelected, setMagnitudeselected] = useState<number[]>([7, 10])
  // armazena a lista de terremotos encontrados pela busca
  const [earthquakes, setEarthquakes] = useState<Earthquake[]>([])
  // armazena o centro do circulo para busca
  const [center, setCenter] = useState<[number, number] | undefined>(undefined)
  // armazena a posicao do usuario
  const [userPosition, setUserPosition] = useState<[number, number] | undefined>(undefined)

  useEffect(() => {
    navigator.geolocation?.getCurrentPosition(position => {
      setUserPosition([position.coords.latitude, position.coords.longitude])
    })
  }, [])

  // realiza a busca de terremotos baseado na magnitude sempre que o usuario escolher outro valor para max e min de magnitude no slider
  useEffect(() => {
    const [minm, maxm] = magnitudesSelected
    const latDLngD = convertLatLngToDegrees(center)

    getEarthquakes(latDLngD, maxm, minm).then(data => setEarthquakes(data))
  }, [magnitudesSelected, center])

  return (
    <div className="page-create-point">
      <div className="content">
        <header><Icon /></header>
        <main>
          <form>
            <fieldset>
              <legend>Busca por ocorrências</legend>
              <div className='field-group'>
                <div className='field'>
                  <Typography id="range-slider" gutterBottom>Maginitude</Typography>
                  <Slider id='slider' value={magnitudes} max={10} step={0.1} onChange={(e, v) => showSlideValue(e, v, setMagnitudes)} onChangeCommitted={(e, v) => handleChange(e, v, setMagnitudeselected)} valueLabelDisplay="auto" marks={[{ value: 0, label: 0 }, { value: 3, label: 3 }, { value: 5, label: 5 }, { value: 7, label: 7 }, { value: 10, label: 10 }]} aria-labelledby="range-slider" getAriaValueText={() => String(magnitudes)} />
                </div>
              </div>
            </fieldset>
            <div className="leaflet-container">
              <Map id='map' center={userPosition} zoom={15} onClick={(e:LeafletMouseEvent) => handleMapClick(e, setCenter)}>
                <TileLayer attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                {
                  center &&
                    <>
                      <Marker position={center} />
                      <Circle center={center} radius={15000} />
                    </>
                }
              </Map>
            </div>
          </form>
          <EarthquakeItemList items={earthquakes} />
        </main>
        <footer></footer>
      </div>
    </div>
  );
}

export default App;
