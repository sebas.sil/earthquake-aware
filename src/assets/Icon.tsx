import React from 'react'
import { FiActivity } from "react-icons/fi";
import { BsBuilding } from "react-icons/bs";

const Icon = () => {
    return (
        <div style={{ display: 'inline-block', position: 'relative' }}>
            <FiActivity textAnchor="middle" alignmentBaseline="middle" style={{ color: 'red', fontSize: 40, strokeWidth: 1, transform: 'rotate(-40deg)' }} />
            <BsBuilding textAnchor="middle" alignmentBaseline="middle" style={{ position: 'absolute', left: '-0.04em', bottom: '0.1em', fontSize: 50, transform: 'rotate(20deg)' }} />
        </div>
    )
}

export default Icon