
class Earthquake {
    private id: string
    private mag: number
    private time: Date
    private type: string
    private url: string
    private place: string
    private alert: string

    constructor(id: string, mag: number, time: number, type: string, url: string, place: string, alert: string) {
        this.id = id
        this.mag = mag
        this.time = new Date(time)
        this.type = type
        this.url = url
        this.place = place
        this.alert = alert
      }

    getId() : string {
        return this.id
    }
    getMag() : number {
        return this.mag
    }
    getTime() : Date {
        return this.time
    }
    getType() : string {
        return this.type
    }
    getUrl() : string {
        return this.url
    }
    getPlace() : string {
        return this.place
    }
    getAlertColor(): string {
        return this.alert
    }
}

export default Earthquake