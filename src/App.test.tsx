import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import EartquakeItemList from './EarthquakeItemList'
import { getEarthquakes } from './service/api'
import { act } from 'react-dom/test-utils';


let container : HTMLDivElement;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
});

test('renders without crashing', () => {
  act(() => {
    ReactDOM.render(<App />, container);
  });
  const slider = container.querySelector('#slider');
  const map = container.querySelector('#map');
  const list = container.querySelector('#list');

  expect(slider).toBeTruthy();
  expect(map).toBeTruthy();
  expect(list).toBeTruthy();
  expect(list?.querySelectorAll('tr')).not.toHaveLength(0);
});

test('is service OK?', () => {
  expect.assertions(1);
  return expect(getEarthquakes(undefined, 10, 7)).resolves.not.toHaveLength(0);
});

test('render itens list', () => {
  return getEarthquakes(undefined, 10, 7).then(array => {
    act(() => {
      ReactDOM.render(<EartquakeItemList items={array} />, container);
    });
  });
});