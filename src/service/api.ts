import { LeafletMouseEvent } from 'leaflet'
import Earthquake from '../Earthquake'

// interface de terremoto, apenas com os atributos que sao utilizados, vindos do servico de monitoramento
interface USGSResponse {
    id: string,
    properties: {
        mag: number,
        time: number,
        type: string,
        url: string,
        place: string,
        alert: string
    }
}

/**
 * Get the earthquakes from the monitoring service based on query parameters
 * It can seek earthquakes of a min and max magnitude range and based on a cilcle of 15km
 * @param latDLngD, latitude and longitude of the circles center in degrees
 * @param maxm, maximum value of the earthquake magnitude (Richter scale)
 * @param minm, minimum value of the earthquake magnitude (Richter scale)
 */
export function getEarthquakes(latDLngD: [number, number] | undefined, maxm: number, minm: number): Promise<Earthquake[]> {
    //TODO descrever melhor o servico e pq alguns terremotos nao sao mostrados (como o 9.5 no chile em 1960)
    return fetch('https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&maxmagnitude=' + maxm + '&minmagnitude=' + minm + (latDLngD ? ('&maxradiuskm=15000&latitude=' + latDLngD[0] + '&longitude=' + latDLngD[1]) : '')
    ).then(response => response.json()
    ).then(data => {
        const es: Earthquake[] = data.features.map((e: USGSResponse) => {
            const { id, properties: { mag, time, type, url, place, alert } } = e
            return new Earthquake(id, mag, time, type, url, place, alert)
        })
        return es
    })
}

/**
 * Convert latitude and longitude to degrees (without seconds)
 * @param latLng array with latitude in the first position and longitude in the seconde
 * @returns [lat, lng], lat in range [-90,90] and lng in range [-180,180]. Both in degrees
 */
export function convertLatLngToDegrees(latLng : [number, number] | undefined) : [number, number] | undefined {
    if(latLng){
        var convertLat = Math.abs(latLng[0]);
        var LatDeg = Math.floor(convertLat);
        var LatMin = (Math.floor((convertLat - LatDeg) * 60));
        var lat = Number(LatDeg+'.'+LatMin)
        if(latLng[0] < 0){
            lat *= -1;
        }
        
        var convertLng = Math.abs(latLng[1]);
        var LngDeg = Math.floor(convertLng);
        var LngMin = (Math.floor((convertLng - LngDeg) * 60));
        var lng = Number(LngDeg+'.'+LngMin)
        if(latLng[0] < 0){
            lng *= -1;
        }
        return [lat, lng]
    }
    return undefined
}

  // mostra o valor no slider quando o usuario desliza o ponto de max ou min
  export function showSlideValue (event: React.ChangeEvent<{}>, newValue: number | number[], setMagnitudes : (maxMinMagnitude: [number, number]) => void) {
    setMagnitudes(newValue as [number, number]);
  };

  // aciona a substituicao do valor selecionado pelo usuario para mandar para o servico de busca
  export function handleChange (event: React.ChangeEvent<{}>, newValue: number | number[], setMagnitudeselected : (maxMinMagnitude: [number, number]) => void) {
    setMagnitudeselected(newValue as [number, number]);
  };

  export function handleMapClick (event: LeafletMouseEvent, setCenter : (position: [number, number]) => void) {
    setCenter([event.latlng.lat, event.latlng.lng])
  }