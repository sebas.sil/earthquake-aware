import React from 'react'
import Earthquake from './Earthquake'

interface Param {
    items: Earthquake[]
}
const EarthquakeItemList: React.FC<Param> = (props) => {

    const dateTimeFormat = new Intl.DateTimeFormat('pt', { year: '2-digit', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' })
    const colors = (color:string) => {
        switch(color){
            case 'yellow': return 'rgba(255,255,0,.2)'
            case 'green': return 'rgba(0,255,0,.2)'
            case 'red': return 'rgba(255,0,0,.2)'
            case null: return 'rgba(77,88,99,.2)' // light gray
            default: return 'rgba(0,0,0,.2)'
        }
        
    }
    return (
        <table id='list' className='items-grid'>
            <thead>
                <tr>
                    <th className='date'>data</th>
                    <th className='mag'>magnitude</th>
                    <th className='local'>lugar</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.items.length === 0 ? <tr><td colSpan={3}>Sem eventos com o filtro selecionado</td></tr> :
                    props.items.map(e => (
                        <tr key={e.getId()} onClick={() => { 'document.location = {e.getUrl()}' }} style={{backgroundColor: colors(e.getAlertColor())}}>
                            <td className='date'>{dateTimeFormat.format(e.getTime())}</td>
                            <td className='mag'>{e.getMag()}</td>
                            <td className='local'>{e.getPlace()}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    )
}

export default EarthquakeItemList