Projeto criado para mostrar eventos de terremoto no mundo com filtro de distancia e magnitude utilzando o serviço de monitoramento [USGS](https://earthquake.usgs.gov).

## Scripts

Alguns scripts automatizados já esta disponíveis:

### `yarn start`

Executa a aplicação em modo de desenvolvimento configurado para:<br />
Abrir [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

Neste modo, ao realizar uma atualização no código, a página é automaticamente recarregada e é possível ver os erros e avisos no console.

### `yarn test`

Executa os scripts de teste no modo interativo.<br />
Mais informações em [running tests](https://facebook.github.io/create-react-app/docs/running-tests).

### `yarn build`

Cria o pacote para implementação no ambiente de produção.<br />
Este cria um pacote do React em modo produção otimizando-o para melhor desempenho.

O pacote é compactado e os nomes dos arquivos tem um hash como parte dele (sempre que for feito um build, um novo hash é gerado - não utilize esses nomes diretamente na aplicação).<br />

Mais informações em [deployment](https://facebook.github.io/create-react-app/docs/deployment).

## Built With

* [VS Code 1.46.1](https://code.visualstudio.com) - IDE de desenvolvimento
* [yarn 1.22.4](https://yarnpkg.com) - Utilizado como gerenciador de dependências
* [nodejs 12.18.1](https://nodejs.org/en/about) - JavaScript runtime built


## Learn More

Mais sobre o aplicações React em [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

Mais sobre o próprio React em [React documentation](https://reactjs.org).
